FROM python:3.9

RUN pip install mpmath scipy

COPY main.py .

CMD ["python", "main.py"]
